#!/usr/bin/env python3
"""
Simple module for downloading shibes and displaying them.
"""

import json
import subprocess
import sys
import requests

def open_image(path):
    """Takes path of image and opens it in the OS default image viewer."""
    image_viewer = {'linux':'xdg-open',
                    'win32':'explorer',
                    'darwin':'open'}[sys.platform]
    subprocess.Popen([image_viewer, path])

def main():
    """Main method"""
    print("> Getting URLs of shibes...")
    shibe_list = requests.get("https://shibe.online/api/shibes?count=3&urls=true&httpsUrls=true")

    jlist = json.loads(shibe_list.text)

    idx = 0
    print("> Downloading shibes...")
    print("> Opening shibes...")
    for img in jlist:
        request_img = requests.get(img, stream=True)
        shibe = "shibe{}.jpg".format(idx)
        if request_img.status_code == 200:
            with open(shibe, 'wb') as shibber:
                for chunk in request_img:
                    shibber.write(chunk)
            idx += 1
        open_image(shibe)


if __name__ == "__main__":
    main()
